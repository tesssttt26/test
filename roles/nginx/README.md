Example of include:

```
  roles:
  - role: nginx
    NGINX_proxy: yes
    NGINX_server_name:   "{{ webhostname }}"
    NGINX_template_name: proxy
    NGINX_proxy_port:    8080
    NGINX_proxy_ip:      127.0.0.1
    NGINX_ssl_letsencypt: true
    NGINX_to_https_redirect: yes
    NGINX_xss_prevention: false
    NGINX_allowed_ips: [ 10.10.10.10, 10.10.10.20, 10.10.10.30, 10.20.20.40, 10.20.20.50 ]
    NGINX_extra_locations:
      - { name: admin, root: "{{ def_web_path }}/{{ webhostname }}/admin" }
    tags:
      - nginx
```
