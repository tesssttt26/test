provider "aws" {
  region     = "us-east-2"
}

resource "aws_key_pair" "test_key" {
  key_name   = "my"
  public_key = file("my.pub")
}


resource "aws_instance" "fpm" {
  ami                    = "ami-013de1b045799b282"
  instance_type          = "t2.micro"
  key_name               = "my"
  vpc_security_group_ids = [aws_security_group.web.id, aws_security_group.ssh.id]
}

resource "aws_instance" "wp" {
  ami                    = "ami-013de1b045799b282"
  instance_type          = "t2.micro"
  key_name               = "my"
  vpc_security_group_ids = [aws_security_group.web.id, aws_security_group.ssh.id]
}

resource "aws_eip" "wp" {
    vpc = true
    instance = aws_instance.wp.id
}

resource "aws_eip" "fpm" {
    vpc = true
    instance = aws_instance.fpm.id
}

resource "aws_security_group" "web" {
  name        = "web security"
  description = "Sec group for test"


  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "web"
  }
}

resource "aws_security_group" "ssh" {
  name        = "default-ssh"
  description = "Security group for nat instances that allows SSH and VPN traffic from internet"


  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "null_resource" "fpm" {
  provisioner "local-exec" {
    command = "sleep 120"
  }
  provisioner "local-exec" {
    command = "sleep 120; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu -i '${aws_instance.fpm.public_dns},' --extra-vars \"NGINX_server_name='${aws_instance.fpm.public_dns}' \" --private-key ~/.ssh/kk fpm.yml -Dvv"
  }
  depends_on = [aws_instance.wp, ]
}

resource "null_resource" "wp" {
  provisioner "local-exec" {
    command = "sleep 120"
  }
  provisioner "local-exec" {
    command = "sleep 120; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu -i '${aws_instance.wp.public_dns},' --extra-vars \"NGINX_server_name='${aws_instance.wp.public_dns}' \" --private-key ~/.ssh/kk wp.yml -Dvv"
  }

}
